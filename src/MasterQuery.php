<?php

namespace ETWSL;

/**
 * ET master server query service.
 *
 * @author Adam Klvač <adam@klva.cz>
 */
class MasterQuery
{

	public const PORT = 27950;
	public const TIMEOUT = 10;
	public const PROTOCOL = 84;

	/**
	 * Queries a master server for the list.
	 * @param string $address
	 * @param int $port
	 * @param int $timeout
	 * @return array
	 */
	public static function read(string $address, int $port = self::PORT, int $timeout = self::TIMEOUT): array
	{
		$delimiter = str_repeat(chr(255), 4) . 'getservers';

		$socket = fsockopen("udp://$address", $port);
		@stream_set_blocking($socket, 0);
		@stream_set_timeout($socket, $timeout);
		@fwrite($socket, $delimiter . ' ' . static::PROTOCOL . " full empty\n");

		$time = time() + $timeout;
		$raw = '';
		while ($time > time() && is_resource($socket)) {
			$raw .= @fgets($socket);
		}

		@fclose($socket);

		$list = [];
		$delimiter .= 'Response';

		foreach (explode('\\', $raw) as $item) {

			if ($item === $delimiter || "EOT$delimiter" === $item || $item === 'EOT') {
				continue;
			}

			if (strlen($item) === 6) {
				$ip = [];
				for ($i = 0; $i < 4; $i++) {
					$ip[] = ord($item[$i]);
				}

				$port = (ord($item[4]) << 8) + ord($item[5]);

				foreach ($ip as $octet) {
					if ($octet < 0 || $octet > 255) {
						continue 2;
					}
				}

				if ($port < 1 || $port > 65535) {
					continue;
				}

				$list[] = implode('.', $ip) . ":$port";
			}

		}

		return $list;
	}

}