<?php

namespace ETWSL;
use Exception;

/**
 * ET server status query service.
 *
 * @author Adam Klvač <adam@klva.cz>
 */
class ServerQuery
{

	public const PORT = 27960;
	public const TIMEOUT = 3;
	public const PROTOCOL = 84;

	/**
	 * Queries a server for status.
	 * @param string $address
	 * @param int $port
	 * @param int $timeout
	 * @return array
	 * @throws Exception
	 */
	public static function read(string $address, int $port = self::PORT, int $timeout = self::TIMEOUT): array
	{
		$socket = fsockopen("udp://$address", $port);
		@stream_set_blocking($socket, 1);
		@stream_set_timeout($socket, $timeout);
		@fwrite($socket, str_repeat(chr(255), 4) . "getstatus\n");

		$raw = @fread($socket, 4096);

		@fclose($socket);

		$header = substr($raw, 0, 19);
		if ($header !== str_repeat(chr(255), 4) . "statusResponse\n") {
			throw new Exception('Invalid or no response from server.');
		}

		$raw = explode("\n", $raw);

		$server = [];
		$key = null;
		foreach (explode('\\', substr($raw[1], 1)) as $value) {
			if ($key === null) {
				$key = $value;
			} else {

				if (preg_match('~\d+~', $value)) {
					$value = (int) $value;
				}

				$server[$key] = $value;
				$key = null;

			}
		}

		if (isset($server['p'])) {
			$server['P'] = $server['p'];
			unset($server['p']);
		}

		$p = str_replace('-', '', @$server['P']); // - empty slot

		$server['players'] = [];
		if (count($raw) > 2) {
			for ($i = 2; $i < count($raw); $i++) {
				if (preg_match('/^(\d+) (\d+) "(.+)"$/', $raw[$i], $matches)) {
					$server['players'][] = [
						'score' => $matches[1],
						'ping' => $matches[2],
						'name' => $matches[3],
						'team' => isset($p[$i - 2]) ? $p[$i - 2] : 3
					];
				}
			}
		}

		return $server;
	}

}