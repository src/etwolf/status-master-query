Wolfenstein: ET server status and master server list query tool
===============================================================

This simple library provides easy ET server status and master server list retrieval. I'm not going to implement any more features, enhancements nor fixes, since the codes does what I expect and I' not aware of any critical issue.

Installation
------------

```
composer require amk/etwsl
```

Getting serverlist
------------------

All you need to know is the hostname of a master server. Currently, there are two reliable master servers:

- `etmaster.idsoftware.com`
- `master0.etmaster.net`

Getting the list is pretty easy!

```php
$servers = ETWSL\MasterQuery::read('etmaster.idsoftware.com');
```

Returned array will contain addresses of indexed servers formatted as `ip:port`.

```php
Array (
	'12.13.14.15:27961',
	'237.12.12.10:27960',
	'237.12.12.10:27800',
)
```

There is about 400 servers these days. **Hey, only 2.60b** (2.60d respectively) are returned (protocol 84). Fork and make the protocol optional, if you care.

Keep in mind, that the fetching list takes few seconds, now there is 10 seconds timeout (third argument). It will work with about 3 - 4 seconds too (less servers may be retured).

Getting serverstatus
--------------------

Server status is returned almost raw, just represented as the array.

```php
$status = ETWSL\ServerQuery::read('237.12.12.10', 27960);
```

This is what the result can look like (it's really up to server):

```php
Array (
	'mod_version' => '3.2.6',
	'g_bluelimbotime' => 20000,
	'g_redlimbotime' => 30000,
	'gamename' => 'etpro',
	'g_gametype' => 3,
	'g_antilag' => 1,
	'g_voteFlags' => 0,
	'g_needpass' => 0,
	'g_friendlyFire' => 1,
	'mapname' => 'sp_delivery_te',
	'protocol' => 84,
	'timelimit' => 20,
	'version' => 'ET 2.60d linux-i386 Feb  6 2012',
	'sv_hostname' => '^1Server hostname ^7with colors',
	'sv_privateClients' => 2,
	'sv_maxclients' => 18,
	'players' => Array (
		Array (
			'name' => '^7hacafrakus^1*',
			'ping' => 48,
			'score' => 187,
			'team' => 2
		),
		Array (
			'name' => '^0slax',
			'ping' => 48,
			'score' => 215,
			'team' => 1
		),
		Array (
			'name' => 'a pervert',
			'ping' => 69,
			'score' => 0,
			'team' => 0
		)
	)
)
```

Licence
-------

Seriously, **Do What The Fuck You Want To**.