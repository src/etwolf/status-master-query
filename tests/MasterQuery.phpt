<?php

/**
 * TEST: MasterQuery class.
 */

require(__DIR__ . '/bootstrap.php');

define('MASTER', 'etmaster.idsoftware.com');

$servers = ETWSL\MasterQuery::read(MASTER, 27950, 3);

foreach ($servers as $server) {
	Tester\Assert::match('%d%.%d%.%d%.%d%:%d%', $server);
}

Tester\Assert::true(count($servers) > 0);