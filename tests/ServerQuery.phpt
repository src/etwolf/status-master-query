<?php

/**
 * TEST: MasterQuery class.
 */

require(__DIR__ . '/MasterQuery.phpt');

foreach ($servers as $server) {

	list ($address, $port) = explode(':', $server);
	$info = ETWSL\ServerQuery::read($address, $port);

	Tester\Assert::true(is_array($info));
	Tester\Assert::same(84, $info['protocol']);
	Tester\Assert::true(isset($info['sv_hostname']));

	break;

}